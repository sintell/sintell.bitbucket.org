#Graph.js
######Graph - map functions on keys, while taking dependencies into consideration
-------

_[Tests](http://sintell.bitbucket.org/graphjs/ "Graphjs up to date unit tests")_

This library can be used for running some functions that depends one after another.
Library will automaticly resolve any dependency, while guaranties that every function will be called once to prevent loss of time and resourses.


###Constructor


`new Graph({functions}, {dependency_list})`
  
###Example

```
var g = new  Graph({
    n : function(args){ return args.xs * 2 },
    n2 : function(args){ return args.n * 2 + args.xs },
    n3 : function(args){ return args.n2 / args.n + args.xs }
},{
    n2 : ['n'],
    n3 : ['n', 'n2']
})
```  

This will create Graph object that watches on three functions.

We can add any parametr using `with()` method like this:

`g.with({xs:1})`

`with()` takes an object that contains any number of arguments, that will be passed to functions on call and returns `this`, so you can call `run()` right after.

Now we can run these functions all at once:
  
 `g.with({xs: 0.2}).run()`, with `xs` as parametr.

Or one by one by specifying name of the function that we want to call in `run()` method:

`g.with({xs:2}).run("n");`

`g.with({xs:12}).run("n2");`


###Disclaimer
This lybrary is still "work in progress", so a lot needs to be done.

###TO DO
- add possibility to run multiple functions by specifying their names in `run()` method
- reset all computable arguments after we've changed any argument by calling `with()` to prevent computation errors