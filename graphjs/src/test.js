graph = new  Graph({
      n : function(args){ return args.xs * 2 },
      n2 : function(args){ return args.n * 2 + args.xs },
      n3 : function(args){ return args.n2 / args.n + args.xs } },
      { n2 : ['n'],n3 : ['n', 'n2']})


graph2 = new  Graph({
    n : function(args){ console.log(args.xs); return args.xs.map(function(el){return el * 2}) },
    n2 : function(args){ console.log(args.n); return args.n.map(function(el){return el * 2 + args.xs[0]}) }
  },{
    n2 : ['n']
  })

test( "Creates new object", function() {
  ok( typeof graph === 'object', "Creates new object" );
});

test("Contains all function passed to constructor", function(){
  propEqual( graph.func, 
    {
      n : function(args){ return args.xs * 2 },
      n2 : function(args){ return args.n * 2 + args.xs },
      n3 : function(args){ return args.n2 / args.n + args.xs }
    }, "Property func should contain all functions passed to constructor" );
});

test("Contains all dependencies passed to constructor", function(){
  propEqual( graph.dep, 
    {
      n2 : ['n'],
      n3 : ['n', 'n2']
    }, "Property deps should contain all dependencies passed to constructor" );
});

test("Successefully performs computation on single number argument", function(){
  deepEqual(graph.with({xs:2}).run(), {n: 4, n2: 10, n3: 4.5}, "No computation errors with unsigned integers");
  deepEqual(graph.with({xs:0.2}).run(), {n: 0.4, n2: 1, n3: 2.7}, "No computation errors with unsigned floats");
  deepEqual(graph.with({xs:-5}).run(), {n: -10, n2: -25, n3: -2.5}, "No computation errors with signed integers");
  deepEqual(graph.with({xs:-0.01}).run(), {n: -0.02, n2: -0.05, n3: 2.49}, "No computation errors with signed floats");
})


test("Successefully performs computation on array of numbers argument", function(){
  deepEqual(graph2.with({xs:[1,2,3]}).run(), {n: [2,4,6], n2: [5,9,13]}, "No computation errors on array of unsigned integers");
  deepEqual(graph2.with({xs:[0,2,-1]}).run(), {n: [0,4,-2], n2: [0,8,-4]}, "No computation errors on array of integers");
  deepEqual(graph2.with({xs:[]}).run(), {n: [], n2: []}, "Successefully handled empty array");
})
