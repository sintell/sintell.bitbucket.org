/*
  Graph - map functions on keys, while taking dependencies into consideration
*/


// Should recieve key-value pairs for each function we need, also can take dependencies and defaults as a params
// Pass calculated params in call like hash

var isFunction = function(fn){
  return Object.prototype.toString.call( fn ) === "[object Function]";
};

var Graph = function(funcs, dep, def){
  for(var name in funcs){
    if(!funcs.hasOwnProperty(name)){ continue; }
    if (!isFunction(funcs[name])) {throw new Error("Not a function");}
   
    if(dep.hasOwnProperty(name)){
      for( var i = 0, _l = dep[name].length; i < _l; i++ ){
        if(dep[name][i].indexOf(name) !== -1){
          throw new Error("Cyclic dependency in " + name);
        }
      }
    }
  }
  this.func = funcs;
  this.dep = dep;
  this.args = def || {};
  this.res = {};
  this.fnames = Object.keys(funcs);
};

/*

  Graph(functions, dependency_list)
  
  var g = new  Graph({
    n : function(args){ return args.xs * 2 },
    n2 : function(args){ return args.n * 2 + args.xs },
    n3 : function(args){ return args.n2 / args.n + args.xs }
  },{
    n2 : ['n'],
    n3 : ['n', 'n2']
  })
*/

/*

  Garph.with({xs: 0.2}).run()

  Graph.with({xs:2}).run("n");

  Graph.with({xs:12}).run("n2");

*/


Graph.prototype.with = function(args) {
  for (var name in args) {
    if( !args.hasOwnProperty(name) ){ continue; }
    this.args[name] = args[name];
  }
  return this;
};


/*
  * If top object has key named after the function then RUN NAMED FUNCTION
  * If top object hasn't keys named after the functions then RUN ALL FUNCTIONS
*/

Graph.prototype.run = function(arg) {
  var keys = [];
  if(Object.prototype.toString.call(arg) === "[object Object]"){
    keys = Object.keys(arg);
  }

  if ( keys[0] in this.fnames ) {
    
    this.with(arg[keys[0]]);
  } else {
    
    this.with(arg);
  } 
  if ( typeof arg == 'undefined' ) {
    
    for ( var fn in this.func ) {
      if ( !this.func.hasOwnProperty(fn) ) { continue; }
      this.res[fn] = this.run(fn);
    }
    return this.res;
  } else {
    if ( typeof this.dep[arg] == 'undefined' ) { 
      
      this.res[arg] = this.func[arg](this.args);
      this.args[arg] = this.res[arg];
    } else {
      
      for ( var i = 0; i < this.dep[arg].length; i++ ) {
        if ( typeof this.args[this.dep[arg][i]] !== 'undefined' ) {  continue;  }
        
        this.res[this.dep[arg][i]] = this.run(this.dep[arg][i]);
      }
      this.res[arg] = this.func[arg](this.args);
      this.args[arg] = this.res[arg];
    }
    return this.res[arg];
  }
};