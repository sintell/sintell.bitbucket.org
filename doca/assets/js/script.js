$(function(){
  onresize = function(){var ha = $(".header__top-art"); ha.height($("html").width()*1042/2000);};
  $('.photos').addCarousel();
  $('.photos__friends').addCarousel();
});

$.fn.addCarousel = function(){
  var carCon = $(this).find('.carousel__container');
  var itemCount = carCon.find('.carousel__item').length;
  var itemWid = parseInt(carCon.find('.carousel__item').width(), 10);

  carCon.css({"width":(itemWid*itemCount+2*(itemCount))+"px"});
  var contWid = parseInt(carCon.width(), 10);
  carCon.css({"left":-(contWid/2 +itemWid/2+1)+"px"});
  console.log(itemWid, itemCount, contWid);

  carPrev = function(e){
    e.preventDefault();
    var carCon = (_el = $(this).nextAll(".carousel__container"), _el.length>0?_el:$(this));

    var curPos = parseInt(carCon.css("left"), 10);
    var item = carCon.find(".carousel__item:last");
  

    carCon.animate( {"left":(curPos+itemWid+2)+"px"},{"duration":300, "queue": false}, function(){

      item.remove();
      carCon.prepend(item);
      carCon.css({"left":curPos+"px"});
    });

  };

  carNext = function(e){
    e.preventDefault();
    var carCon = (_el = $(this).nextAll(".carousel__container"), _el.length>0?_el:$(this));
    var curPos = parseInt(carCon.css("left"), 10);
    var item = carCon.find(".carousel__item:first");

    carCon.animate( {"left":(curPos-itemWid-2)+"px"}, {"duration":300, "queue": false}, function(){
      item.remove();
      carCon.append(item);
      carCon.css({"left":curPos+"px"});
    });
  };

  $(this).find(".carousel__prev").on("click", carPrev);
  $(this).find(".carousel__container").on("swiperight", carPrev);
  $(this).find(".carousel__next").on("click", carNext);
  $(this).find(".carousel__container").on("swipeleft", carNext);
};

ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.797867, 37.799289],
            zoom: 16
        }),

        // Создаем геообъект с типом геометрии "Точка".
        docaMark = new ymaps.Placemark([55.799511,37.800168],{
                hintContent: 'D.O.C.A'
            },{
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            iconImageHref: 'http://i.minus.com/ibir005MDmFKxY.png',
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-3, -42]
        });

        myMap.geoObjects
        .add(docaMark);






      (function(){
        window.scrollease = {
            getPos: function( elm ){
                var x = 0, y = 0;
                while( elm !== null ) {
                    x += elm.offsetLeft;
                    y += elm.offsetTop;
                    elm = elm.offsetParent ;
                }
                return {x:x, y:y};
            },
            damper:function(rfnAction, rfnDone, duration){
                var interval,
                    startTime = new Date().getTime();

                interval = setInterval( function(){
                    var pos, 
                        t,
                        now = new Date().getTime();

                    t = now - startTime;
                    if(t >= duration){
                        clearInterval(interval);
                        rfnDone.call(scrollease);
                    }else{
                        t = 2 * t / duration;
                        if (t < 1) {
                            pos = 0.5*t*t*t*t;
                        }else{
                            t -= 2;
                            pos = -0.5 * (t*t*t*t - 2);
                        }                       
                        rfnAction.call(scrollease, pos);
                    }
                }, 15 );
            },
            scrollTo: function( a ){
                try{
                    var endName = a.href.split('#')[1],
                        endElm = document.getElementById(endName),
                        start = isNaN(window.pageYOffset) ? 
                            document.documentElement.scrollTop :
                            window.pageYOffset,
                        end = scrollease.getPos(endElm).y,
                        length = scrollease.getPos(endElm).y - start;

                    this.damper(function(pos){
                        //when moving
                        window.scrollTo(0, start + length * pos);
                    }, function(){
                        //when done
                        window.location.hash = endName;
                    },
                    //duration
                    Math.max( Math.abs( Math.round(length / 3) ), 1200));
                    return false;
                }catch(e){
                    return true;
                }
            }
        };
    })();
}