var EventStorage = (function() {
    "use strict";

    function EventStorage( autoLoad ) {
        // enforces new
        if (!(this instanceof EventStorage)) {
            return new EventStorage( autoLoad );
        }
        // constructor body
        this.Evs = {};
        this.NamesIndex = {};
        this.DatesIntex = {};
        this.EvsIndex = {};
        this.monthNamesExt = ["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"];
        if ( autoLoad ) this.load();
    }

    EventStorage.prototype.newEvent = function( date, title, participants, desc ) {
        // method body
        var year = date.getFullYear(),
          month = date.getMonth(),
          day = date.getDate();
        var ev = {
          title: title,
          participants: participants,
          desc: desc,
          _lookup: (title + "," + year + "." +  month+1 + "." + day + "," + day + " " + this.monthNamesExt[month] + "," + participants + "," + desc).toLowerCase()
        };

        this.Evs[year + "." +  month + "." + day] = ev;
        this.save();
    };
    EventStorage.prototype.deleteEvent = function( date ) {
        // method body
        var year = date.getFullYear(),
        month = date.getMonth(),
        day = date.getDate();

        delete this.Evs[year + "." +  month + "." + day];
        this.save();
    };
    EventStorage.prototype.save = function() {
      // method body
      var l = window.localStorage;
      if ( l != undefined )
        l.setItem("Calendar", JSON.stringify(this.Evs));
    };
    EventStorage.prototype.load = function() {
      // method body
      var l = window.localStorage;
      if ( l != undefined )
        this.Evs = JSON.parse(l.getItem( "Calendar" )) || {};
    };
    EventStorage.prototype.getEvent = function( date ) {
      // method body
      var year = date.getFullYear(),
        month = date.getMonth(),
        day = date.getDate();
      return this.Evs[year + "." + month + "." + day];
    };

    EventStorage.prototype.search = function( query ) {
      var events = [];
      var dates = [];
      for (var ev in this.Evs) {
        if (this.Evs.hasOwnProperty(ev)) {
          if ( this.Evs[ev]._lookup.match( query ) ) {
            dates.push( [ev.split('.')[0], ev.split('.')[1], ev.split('.')[2] ] );
          }
        }
      }
      dates.sort();
      for (var i = 0; i < dates.length; i++) {
        var year = dates[i][0],
        month = dates[i][1],
        day = dates[i][2];
        var t = this.Evs[year + "." + month + "." + day];
        t.date = year + "." + month + "." + day;
        events.push( t );
      }
      return events;
    };

    return EventStorage;

}());


var Calendar = (function() {
  "use strict";
  
  var elementBuildHelper = function( element ){
    var el = document.createElement( element.name );
    for (var attr in element.attrs) {
      if (element.attrs.hasOwnProperty(attr)) {
        el.setAttribute( attr, element.attrs[attr] );
      }
    }
    if ( element.text && el.textContent != undefined ) el.textContent = element.text;
    return el;
  };

  function Calendar( node ) {
    // enforces new
    if (!(this instanceof Calendar)) {
        return new Calendar( node );
    }
    // constructor body
    this.dayNames = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
    this.monthNames = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
    this.monthNamesExt = ["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"];
    this.eventStorage = new EventStorage( true );


    

    var calendarNode = node || "calendar";

    this.calendarElem = document.getElementById( calendarNode );

    var calendarControllsEl = elementBuildHelper( {
      name: "div", attrs: {"id":"calendar-controlls", "class": "calendar-controlls"}
    } );

    calendarControllsEl.appendChild(elementBuildHelper( {
      name: "input", attrs: {"id":"dec-month", "type": "button", "value":"◂"}
    } ));
    calendarControllsEl.appendChild(elementBuildHelper( {
      name: "span", attrs: {"id":"current-date-text", "class": "current-date-text"}
    } ));
    calendarControllsEl.appendChild(elementBuildHelper( {
      name: "input", attrs: {"id":"inc-month", "type": "button", "value":"▸"}
    } ));
    calendarControllsEl.appendChild(elementBuildHelper( {
      name: "input", attrs: {"id":"curr-month", "type": "button", "value":"Сегодня"}
    } ));

    this.calendarElem.appendChild( calendarControllsEl );
    this.calendarElem.appendChild( this.buildCalendarTable() );

    document.getElementById( "current-date-text" ).innerHTML = this.monthNames[this.m] + " " + this.y;

    var decMonthElem = document.getElementById( "dec-month" );
    var incMonthElem = document.getElementById( "inc-month" );
    var currMonthElem = document.getElementById( "curr-month" );
    var createFastEvent = document.getElementById( "create-fast-event" );
    var addEvent = document.getElementById( "add-event" );
    var closeButtons = document.getElementsByClassName( "close-button" );
    var calendarCells = document.getElementsByTagName( "td" );
    var searchField = document.getElementById( "search-field" );

    var that = this;
    var decMonthHandler, incMonthHandler, currMonthHandler,
        addEventHandler, closeButtonHandler, createFastEventHandler,
        calendarCellClickHandler, newCalendarEventHandler, delCalendarEventHandler,
        searchInputHandler;

    decMonthHandler = function(){
      if ( --that.m < 0 ) {
        that.m = 11;
        that.y--;
      }
      document.getElementById("calendar").replaceChild( that.buildCalendarTable( that.getStartingDate( that.y, that.m ) ), document.getElementById("calendar-table") );
      document.getElementById("current-date-text").textContent = that.monthNames[that.m] + " " + that.y;
    };
    
    incMonthHandler= function(){
      that.m++;

      if ( that.m > 11 ) {
        that.m = 0;
        that.y++;
      }

      document.getElementById("calendar").replaceChild( that.buildCalendarTable( that.getStartingDate( that.y, that.m ) ), document.getElementById("calendar-table") );
      document.getElementById("current-date-text").textContent = that.monthNames[that.m] + " " + that.y;
    };

    currMonthHandler = function(){
      document.getElementById("calendar").replaceChild( that.buildCalendarTable( that.getStartingDate() ), document.getElementById("calendar-table") );
      
      document.getElementById("current-date-text").innerHTML = that.monthNames[that.m] + " " + that.y;
    };

    addEventHandler = function( e ){
      document.getElementById("new-event-popup").setAttribute("style","display:block;");
    };

    closeButtonHandler = function( e ){
      e.preventDefault();
      this.parentNode.setAttribute("style","display:none");
    };

    createFastEventHandler = function ( e ) {
      var text = document.getElementById('desc-fast-event').value;
      text = text.split(',');
      var date = text[0].match(/(\d\d?) (\W*) ?(\d\d\d\d)?/);

      switch( date[2].trim().toLowerCase() ) {
        case "января": date[2] = 0; break;
        case "февраля": date[2] = 1; break;
        case "марта": date[2] = 2; break;
        case "апреля": date[2] = 3; break;
        case "мая": date[2] = 4; break;
        case "июня": date[2] = 5; break;
        case "июля": date[2] = 6; break;
        case "августа": date[2] = 7; break;
        case "сентября": date[2] = 8; break;
        case "октября": date[2] = 9; break;
        case "ноября": date[2] = 10; break;
        case "декабря": date[2] = 11; break;
      }

      that.eventStorage.newEvent(new Date((date[3] || that.y),date[2],date[1]), text[2], "", "");
      document.getElementById('calendar-table').innerHTML =
        that.buildCalendarTable( that.getStartingDate( that.y, that.m ) );
    };

    calendarCellClickHandler = function ( element ) {

      var selectedElements = that.calendarElem.getElementsByClassName('selected');
      for (var i = 0; i < selectedElements.length; i++) {
        selectedElements[i].setAttribute( "class", selectedElements[i].getAttribute("class").replace(/selected/, "") );
      };

      element.setAttribute("class", element.getAttribute("class") + " selected");
      var eventDiv = document.createElement( "div" );
      var evDate = new Date( element.getAttribute( "data-day" ) );
      var evDateArr = [evDate.getDate(),(evDate.getMonth()+1),evDate.getFullYear()];
      var ev = that.eventStorage.getEvent( evDate );

      eventDiv.setAttribute( "id", "calendar-event" );
      var y = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
      var eventDivStyle =
        ( element.getBoundingClientRect().right + 10 < window.screen.width / 2 + 100 ?
          "left:" + (element.getBoundingClientRect().right + 10) + "px; " :
          "left:" + ( element.getBoundingClientRect().left - 10 - 297) + "px; " ) +
        ( element.getBoundingClientRect().top + 10 < window.screen.height / 2 ?
          "top:" + (element.getBoundingClientRect().top + y - 10) + "px;" :
          "top:" + (element.getBoundingClientRect().bottom - 10 - 366  + y ) + "px;" );
      var arrowClass =
        ( element.getBoundingClientRect().top + 10 < window.screen.height / 2 ?
          "top" :
          "bottom" ) +
        ( element.getBoundingClientRect().right + 10 < window.screen.width / 2 + 100 ?
          "-left" :
          "-right" );

      eventDiv.setAttribute( "class", "calendar-div popup arrow " + arrowClass );
      eventDiv.setAttribute( "style", eventDivStyle);

      if ( ev ) {
        eventDiv.innerHTML = "<div class='title'>" + ev.title + "</div>" +
          "<a class='close-button' href='#'>&times;</a>" +
          "<div class='date' >" + evDate.getDate() + " " + that.monthNamesExt[evDate.getMonth()] +  "</div>" +
          "<input id='ev-participants' class='ev-participants' type='text' placeholder='Имена участников' value='" + ev.participants + "'/>" +
          "<textarea id='ev-desc' class='ev-desc' cols='30' rows='10' placeholder='Описание'>" + ev.desc + "</textarea>" +
          "<input type='button' id='new-cal-event' value='Сохранить'/>" +
          "<input type='button' id='del-cal-event' value='Удалить' />";
      } else {
        eventDiv.innerHTML = "<input id='ev-title' class='ev-title' type='text' placeholder='Событие' />" +
          "<a class='close-button' href='#'>&times;</a>" +
          "<input id='ev-date' class='ev-date' type='text' placeholder='День, месяц, год' value='" +
          evDateArr.join('.') + "' />" +
          "<input id='ev-participants' class='ev-participants' type='text' placeholder='Имена участников' />" +
          "<textarea id='ev-desc' class='ev-desc' cols='30' rows='10' placeholder='Описание'></textarea>" +
          "<input type='button' id='new-cal-event' value='Сохранить'/>" +
          "<input type='button' id='del-cal-event' value='Удалить' />";
      }

      eventDiv.getElementsByTagName('a')[0].addEventListener( 'click', function( e ){
        e.preventDefault();
        element.setAttribute( "class", element.getAttribute("class").replace(" selected", "") );
        document.getElementsByTagName( "body" )[0].removeChild( eventDiv );
      }, false );

      var div = document.getElementById( 'calendar-event' );
      if ( div ) document.getElementsByTagName( "body" )[0].removeChild( div );
      document.getElementsByTagName( "body" )[0].appendChild( eventDiv );
    };

    newCalendarEventHandler = function ( e ) {
      var elem = that.calendarElem.getElementsByClassName('selected')[0];
      var ev = {
        title: e.target.parentElement.getElementsByClassName('ev-title')[0].value,
        date: e.target.parentElement.getElementsByClassName('ev-date')[0].value.split('.').reverse(),
        participants: e.target.parentElement.getElementsByClassName('ev-participants')[0].value,
        desc: e.target.parentElement.getElementsByClassName('ev-desc')[0].value
      };

      that.eventStorage.newEvent( new Date( ev.date[0], ev.date[1]-1, ev.date[2] ), ev.title, ev.participants, ev.desc );
      document.getElementById("calendar").replaceChild( that.buildCalendarTable( that.getStartingDate( that.y, that.m ) ),
        document.getElementById("calendar-table") );


      elem.setAttribute( "class", elem.getAttribute("class").replace(/selected/, "") );
      document.getElementsByTagName( "body" )[0].removeChild( e.target.parentElement );
    };

    delCalendarEventHandler = function ( e ) {
      var elem = that.calendarElem.getElementsByClassName('selected')[0];
      that.eventStorage.deleteEvent( new Date( elem.getAttribute('data-day') ) );

      elem.setAttribute( "class", elem.getAttribute("class").replace(/selected/, "") );
      document.getElementsByTagName( "body" )[0].removeChild( e.target.parentElement );
      document.getElementById("calendar").replaceChild( that.buildCalendarTable( that.getStartingDate( that.y, that.m ) ),
        document.getElementById("calendar-table") );
    };

    searchInputHandler = function ( e ) {
      var query = e.target.value.toLowerCase();
      var searchPopup = document.getElementById( 'search-popup' );
      var searchPopupString = "<a class='close-button' href='#'>&times;</a>";
      if ( query.length < 1) {
        searchPopup.setAttribute("style","display:none;");
        return;
      }

      if ( query.length > 0 ) {
        searchPopup.setAttribute("style","display:block;");
        var results = that.eventStorage.search( query );

        for (var i = 0; i < results.length; i++) {
          var monthName = that.monthNamesExt[results[i].date.split('.')[1]];
          var date = {day:results[i].date.split('.')[2], month:monthName.replace(monthName[0],monthName[0].toUpperCase())};
          searchPopupString += "<div><h2>" + results[i].title +"</h2><div>" + date.day + " " + date.month + "</div>" + "</div>";
        }
        searchPopup.innerHTML = searchPopupString;
        searchPopup.getElementsByClassName('close-button')[0].addEventListener( 'click', function( e ){
          e.preventDefault();
          this.parentNode.setAttribute("style","display:none");
        }, false );
      }
    };

    decMonthElem.addEventListener( 'click', decMonthHandler, false );
    incMonthElem.addEventListener( 'click', incMonthHandler, false );
    currMonthElem.addEventListener( 'click', currMonthHandler, false );
    addEvent.addEventListener( 'click', addEventHandler, false ); 
    createFastEvent.addEventListener( 'click', createFastEventHandler, false );
    searchField.addEventListener( 'keyup', searchInputHandler, false );
    
    searchField.addEventListener( 'blur', function( e ){
      document.getElementById( 'search-popup' ).setAttribute("style","display:none;");
    }, false );

    // фикс для добавления live events для ячеек календаря
    this.calendarElem.addEventListener( 'click', function( e ){
      if( e.target.nodeName == 'TD' ){calendarCellClickHandler( e.target );}
      else if( e.target.parentNode.nodeName == 'TD' ){calendarCellClickHandler( e.target.parentNode );}
      else if( e.target.parentNode.parentNode.nodeName == 'TD' ){calendarCellClickHandler( e.target.parentNode.parentNode );}

    }, false );

    document.getElementsByTagName('body')[0].addEventListener( 'click', function( e ){
      switch( e.target.getAttribute('id') ) {
        case "new-cal-event" : newCalendarEventHandler( e ); break;
        case "del-cal-event" : delCalendarEventHandler( e ); break;
      }
    }, false );
    
    for (var j = closeButtons.length - 1; j >= 0; j--) {
      closeButtons[j].addEventListener( 'click', closeButtonHandler, false );
    }

  }
  Calendar.prototype.daysInMonth = function( year, month ) {
    var currentDate = new Date();
    var MILLIS_IN_DAY = 84000000;
    if ( year == null ) year = currentDate.getFullYear();
    if ( month == null ) month = currentDate.getMonth();
    //  вычисляем количество дней в месяце как разницу в днях между 1-м числом следующего месяца и текущего
    return Math.floor( ( new Date( year, month + 1 ) - new Date( year, month ) )/ MILLIS_IN_DAY);
  };

  Calendar.prototype.getStartingDate = function( year, month ) {
    // method body
    var currentDate = new Date();
    if ( year == null ) year = currentDate.getFullYear();
    if ( month == null ) month = currentDate.getMonth();

    this.m = month;
    this.y = year;

    var startingDate = new Date( year, month ); //  получаем первый день месяца
    //  получаем дату для первого дня недели (понедельника)
    if ( startingDate.getDay() !== 0 ) {
      startingDate.setDate( startingDate.getDate() - startingDate.getDay() + 1);
    } else {
      startingDate.setDate( startingDate.getDate() - 6);
    }


    return startingDate;
  };
  Calendar.prototype.buildCalendarTable = function( startingDate ) {
    // method body

    var firstWeek = true;
    var calendarTableEl = elementBuildHelper( { name:"table", attrs:{"id":"calendar-table"} } ); //"<table id='calendar-table'><tr>";
    var calendarRows = [], calendarCellEl; //"<table id='calendar-table'><tr>";

    if ( startingDate == null ) startingDate = this.getStartingDate();
    
    var d = startingDate;

    var cellCount = this.daysInMonth( this.y, this.m ) + ( new Date(this.y, this.m).getDay() || 7 );
    if ( cellCount > 28 && cellCount <= 36 ) cellCount = 35;
    if ( cellCount > 36 ) cellCount = 42;

    for (var i = 0; i < cellCount; i++) {
      var classes = [];
      var currentDate = new Date();
      var currentDateStr = d.toDateString();
      var ev = this.eventStorage.getEvent( d );

      if ( i > 6 ) firstWeek = false;
  
      if ( d.getDate() == currentDate.getDate() &&
          d.getMonth() == currentDate.getMonth() &&
          d.getFullYear() == currentDate.getFullYear()) classes.push( "today" );
      if ( ev ) classes.push( "event" );

      if ( i % 7 === 0 ) calendarRows.push( elementBuildHelper( { name:"tr", attrs:{} } ) );

      calendarCellEl = elementBuildHelper( { name:"td", attrs:{"class": classes.join(" "), "data-day":currentDateStr} } );
      calendarCellEl.textContent = firstWeek ?
        this.dayNames[d.getDay()] + ", " +d.getDate() :
        d.getDate();

      if ( ev ) { 
        classes.push( "event" );
        calendarCellEl.appendChild( elementBuildHelper( { name:"div", attrs:{}, text:ev.title } ) );
        calendarCellEl.appendChild( elementBuildHelper( { name:"div", attrs:{}, text:ev.participants } ) );
      }
      calendarRows[calendarRows.length-1].appendChild( calendarCellEl );

      d.setDate( d.getDate() + 1 );
    }
    for (var j = 0; j < calendarRows.length; j++) {
      calendarTableEl.appendChild( calendarRows[j] );
    }
    return calendarTableEl;
  };

  return Calendar;

}());

window.onload = function(){
  var calendar = new Calendar('calendar');
};